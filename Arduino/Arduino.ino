//####### CONFIGURATION ITEMS GO HERE #######

#define REPEATER_RX_STATE LOW //The state to detect that the repeater is receiving
#define CROSSLINK_RX_STATE LOW //The state to detect that the crosslink is receiving - IF NO CROSSLINK IS CONNECTED SET TO "LOW" otherwise the pin will float
#define PRE_TAIL_DELAY 750 //Tail length in ms
#define CHECK_DELAY 100 //Time to check to see if an action needs to be taken  - Increasing this will make logs more readable in real time, lowering this will increase the response of the repeater.
#define QSO_TIMEOUT 30000 //Timeout in ms
#define IDENT_ACTIVE_INTERVAL 600000 //Ident interval since last activity in ms
#define IDENT_INACTIVE_INTERVAL 1800000 //Ident interval with no activity since last ident in ms
#define TONE_FREQ 123 //The CTCSS Tone to transmit when repeating audio. The controller will not transmit a tone when identing so that the crosslink (or any other crosslinks) can have tone decode enabled and not hear and repeat/link the idents.
#define CALLSIGN "VK2RKG" //The callsign of the repeater
#define CW_TONE_FREQ 1000 //The Audio Frequency for the CW Messages
#define CW_SPEED 20 //The WPM Speed of the CW Messages

//####### END CONFIGURATION DO NOT EDIT ANYTHING BELOW THIS LINE #######

#include <cww_MorseTx.h>

#define PIN_REPEATER_TX_STATUS 2
#define PIN_REPEATER_RX_STATUS 3
#define PIN_CROSSLINK_TX_STATUS 4
#define PIN_CROSSLINK_RX_STATUS 5
#define PIN_MANUAL_TX_STATUS 6

#define PIN_REPEATER_TX 7
#define PIN_CROSSLINK_TX 8

#define PIN_IDENT 9
#define PIN_TONE 10

#define PIN_MANUAL 11
#define PIN_REPEATER_RX 12
#define PIN_CROSSLINK_RX 13

//These are unused at present - for future development like muting audio on the Rx when the Crosslink is transmitting, or detecting CTCSS tones via a discriminator input.
#define PIN_REPEATER_TX_AUDIO A0
#define PIN_REPEATER_RX_AUDIO A1
#define PIN_CROSSLINK_TX_AUDIO A2
#define PIN_CROSSLINK_RX_AUDIO A3
#define PIN_REPEATER_DISCRIMINATOR_AUDIO A4

cww_MorseTx repeater_morseTx(PIN_MANUAL_TX_STATUS, CW_SPEED, PIN_IDENT, CW_TONE_FREQ);

long last_ident_time = 0;
long qso_start_time = 0;

void setup() {
  Serial.begin(115200);
  int serialWait = 0;
  while(!Serial) {
    serialWait += 1;
    delay(100);
    if (serialWait == 50) {
      break;
    }
  }
  //Start a serial connection
  logMessage((String("VK2MES Repeater Controller starting for ") + String(CALLSIGN)).c_str());
  logMessage("Configuring output pins");
  //define the output pins
  pinMode(PIN_REPEATER_TX_STATUS, OUTPUT);
  pinMode(PIN_REPEATER_RX_STATUS, OUTPUT);
  pinMode(PIN_CROSSLINK_TX_STATUS, OUTPUT);
  pinMode(PIN_CROSSLINK_RX_STATUS, OUTPUT);
  pinMode(PIN_MANUAL_TX_STATUS, OUTPUT);
  pinMode(PIN_REPEATER_TX, OUTPUT);
  pinMode(PIN_CROSSLINK_TX, OUTPUT);
  pinMode(PIN_IDENT, OUTPUT);
  pinMode(PIN_TONE, OUTPUT);

  //define the input pins
  logMessage("Configuring input pins");
  pinMode(PIN_MANUAL, INPUT_PULLUP);
  if (REPEATER_RX_STATE == LOW) {
    logMessage("Repeater Rx pin state set to LOW");
    pinMode(PIN_REPEATER_RX, INPUT_PULLUP);
  } else {
    logMessage("Repeater Rx pin state set to HIGH");
    pinMode(PIN_REPEATER_RX, INPUT);
  }

  if (CROSSLINK_RX_STATE == LOW) {
    logMessage("Crosslink Rx pin state set to LOW");
    pinMode(PIN_CROSSLINK_RX, INPUT_PULLUP);
  } else {
    logMessage("Crosslink Rx pin state set to LOW");
    pinMode(PIN_CROSSLINK_RX, INPUT);
  }

  //Finally send an ident out as we've just come online
  logMessage("Sending first ident");
  sendIdent();
  logMessage("Finish Config");
}

void loop() {
  checkIdent();
  checkRepeater();
  checkCrosslink();
  checkManual();
  delay(CHECK_DELAY);
}

void setTone(){
  logMessage("TX: Setting tone to TONE_FREQ Hz");
  tone(PIN_TONE, TONE_FREQ);
}

void resetTone() {
  logMessage("TX: Resetting tone");
  noTone(PIN_TONE);
}

void repeaterTx() {
  logMessage("TX: Keying up repeater");
  digitalWrite(PIN_REPEATER_TX_STATUS, HIGH);
  digitalWrite(PIN_REPEATER_TX, HIGH);
}

void crosslinkTx() {
  logMessage("TX: Keying up crosslink");
  digitalWrite(PIN_CROSSLINK_TX_STATUS, HIGH);
  digitalWrite(PIN_CROSSLINK_TX, HIGH);
}

void resetTx() {
  logMessage("Tx: Transmission end");
  digitalWrite(PIN_REPEATER_TX_STATUS, LOW);
  digitalWrite(PIN_REPEATER_TX, LOW);
  digitalWrite(PIN_CROSSLINK_TX_STATUS, LOW);
  digitalWrite(PIN_CROSSLINK_TX, LOW);
  digitalWrite(PIN_MANUAL_TX_STATUS, LOW);
}

void sendTimeout() {
  logMessage("TX: Sending timeout morse");
  repeater_morseTx.send((String(CALLSIGN) + String(" TIMEOUT")).c_str());
  delay(PRE_TAIL_DELAY);
}

void sendIdent() {
  logMessage("TX: Sending ident morse");
  repeater_morseTx.send(CALLSIGN);
}

void checkManual() {
  if (digitalRead(PIN_MANUAL) == LOW) {
    logMessage("DETECT: Manual button press");
    logMessage("TX: Manual transmission start");
    delay(5000);
    if (digitalRead(PIN_MANUAL) == LOW) {
      logMessage("DETECT: Manual button long press");
      repeaterTx();
      crosslinkTx();
      setTone();
      digitalWrite(PIN_MANUAL_TX_STATUS, HIGH);
      while(true) {
        logMessage("Tx: Manual transmission in progress");
        if (digitalRead(PIN_MANUAL) == LOW) {
          logMessage("DETECT: Manual button press");
          delay(5000);
          if (digitalRead(PIN_MANUAL) == LOW) {
            logMessage("DETECT: Manual button long press");
            resetTone();
            sendIdent();
            setTone();
          } else {
            resetTone();
            checkIdent();
            break;
          }
        }
        delay(CHECK_DELAY);
      }
    } else {
      sendIdent();
    }
    logMessage("DETECT: Manual tranmission end");
    resetTx();
  }
}

void checkIdent() {
  logMessage("DETECT: Checking ident interval");
  if ( (((millis() - last_ident_time) > IDENT_ACTIVE_INTERVAL) && (qso_start_time > last_ident_time)) || ((millis() - last_ident_time) > IDENT_INACTIVE_INTERVAL) )
  {
    logMessage("DETECT: Ident intverval Exceeded");
    last_ident_time = millis();
    repeaterTx();
    crosslinkTx();
    delay(PRE_TAIL_DELAY);
    sendIdent();
    delay(PRE_TAIL_DELAY);
    resetTx();
  }
}

void checkTimeout(bool crosslink = false) {
  if ((millis() - qso_start_time) > QSO_TIMEOUT) {
    logMessage("DETECT: Transmit timeout Exceeded");
    resetTone();
    sendTimeout();
    resetTx();
    int pinToRead;
    int pinState;
    int statusPin;
    if (!crosslink) {
      pinToRead = PIN_REPEATER_RX;
      pinState = REPEATER_RX_STATE;
      statusPin = PIN_REPEATER_RX_STATUS;
    } else {
      pinToRead = PIN_CROSSLINK_RX;
      pinState = CROSSLINK_RX_STATE;
      statusPin = PIN_CROSSLINK_RX_STATUS;
    }
    logMessage("Tx: Transmit inhibited");
    while (digitalRead(pinToRead) == pinState) {
      logMessage("Tx: Transmit inhibited, Rx still in progress");
      checkIdent();
      digitalWrite(statusPin, LOW);
      delay(PRE_TAIL_DELAY);
      digitalWrite(statusPin, HIGH);
      delay(PRE_TAIL_DELAY);
    }
  }  
}

void checkRepeater() {
  logMessage("DETECT: Checking repeater Rx");
  if (digitalRead(PIN_REPEATER_RX) == REPEATER_RX_STATE) {
    logMessage("DETECT: Repeater receiving");
    qso_start_time = millis();
    digitalWrite(PIN_REPEATER_RX_STATUS, HIGH);
    setTone();
    repeaterTx();
    crosslinkTx();
    while (digitalRead(PIN_REPEATER_RX) == REPEATER_RX_STATE) {
      logMessage((String("Tx: Receiving for ") + String((millis() - qso_start_time))).c_str());
      checkTimeout();
      delay(CHECK_DELAY);
    }
    digitalWrite(PIN_REPEATER_RX_STATUS, LOW);
    resetTone();
    checkIdent();
    delay(PRE_TAIL_DELAY);
    resetTx();
  }
}

void checkCrosslink() {
  logMessage("DETECT: Checking crosslink Rx");
  if (digitalRead(PIN_CROSSLINK_RX) == CROSSLINK_RX_STATE) {
    logMessage("DETECT: Crosslink receiving");
    qso_start_time = millis();
    digitalWrite(PIN_CROSSLINK_RX_STATUS, HIGH);
    setTone();
    repeaterTx();
    while (digitalRead(PIN_CROSSLINK_RX) == CROSSLINK_RX_STATE) {
      logMessage("Tx: Crosslink receiving");
      checkTimeout();
      delay(CHECK_DELAY);      
    }
    digitalWrite(PIN_CROSSLINK_RX_STATUS, LOW);
    resetTone();
    checkIdent();
    delay(PRE_TAIL_DELAY);
    resetTx();
  }
}

void logMessage(char message[]) {
  if (Serial) {
    if (Serial.availableForWrite() >= strlen(message)) {
      Serial.println(String(message));
    }
  }
}