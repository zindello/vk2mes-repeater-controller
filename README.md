# vk2mes-repeater-controller

## Name
VK2MES Arduino Based Repeater Controller

## Description
This is a quick Arduino Leonardo Shield and code that I knocked together for a repeater controller. It will probably work on other hardware revisions as well but a Leonardo is what I had on hand.

It features:
- Keying a repeater
- Integration of a crosslink to a repeater
- Morse idents:
    - Active intervals while the repeater is in use
    - Longer standby interval when the repeater is idling
- LEDs for Repeater Rx/Tx, Crosslink Tx/Tx & Manual Tx
- HIGH or LOW side detection of receiver signals
- Configurable time out timer with morse time out message
- Configurable morse speed
- Configurable morse audio frequency
- Configurable callsign
- Configurable loop timers
- Configurable pre/post Tx delay
- Configurable CTCSS tone frequency
- CTCSS encode on voice transmissions - off for idents (to prevent idents coming through on crosslinks)
- Manual triggering
    - Idents
    - Transmit (for transmitting via the aux audio input)
- Passive audio mixer featuring
    - Repeater Rx Audio
    - Repeater Tx Audio
    - Crosslink Rx Audio
    - Crosslink Tx Audio
    - Morse Audio Level
    - CTCSS Audio Level
    - Auxiliary Audio Level

## Shield Assembly

R11, R12, R13, R14, R15, R21, R23 - 250k Trimpots  
R16, R17, R18, R19, R20, R22, R24 - 25k through hole 1/8w resistors 
R25, R26 - 1k through hole 1/8w resistors  
R1, R2, R3, R4, R5, R6, R9, R10 - 10k through hole 1/8w resistors  
R28, R29, R30, R31, R32 - Resistors in the 120-200 ohm range - these should be selected for the correct current draw from 5v for the LEDs fitted  
D1, D2, D3, D4, D5 - Indicator LEDS - your choice. I like high intensity 5mm LEDs but it's up to you. Note the values required for the dropping resistors change depending on colour  
T1, T2, T3, T4, T5, T6 - 2N2222 NPN transistors  
R27/C2 - These need to be chosen to provide a 300Hz (Maximum) cut-off filter for the discriminator input. This input is experimental and not yet implemented  
C1, C4, C5, C6, C7, C8 - 3.3uf "Yellow" capacitors  
S1, S2 - Microswitches  
X1 - DB15F PCB Mount Right Angle connector  
X2 - 3.5mm Switched Stereo socket (For input from a computer or the like)  
JP1 - 2.5mm 2x2 pin headers for jumpering of audio from the socket - these can alternately be soldered as permanent links  

## Usage

Build the shield, and connect the following pins on the DB15 connector to your repeater  

Pin 1 - Ground  
Pin 2 - Ground  
Pin 3 - Repeater Receive Audio  
Pin 4 - Repeater Transmit Audio  
Pin 5 - Ground  
Pin 6 - Vin: +7 -> +15v  (Buyer beware, this uses the direct regulator on the Arduino)
Pin 7 - Repeater Rx Detect  
Pin 8 - Repeater Tx  
Pin 9 - Ground  
Pin 10 - Crosslink Discriminator Input  
Pin 11 - Ground  
Pin 12 - Crosslink Transmit Audio  
Pin 13 - Crosslink Receive Audio  
Pin 14 - Crosslink Rx Detect  
Pin 15 - Crosslink Tx  

You can connect a stereo line source to the 3.5mm jack for transmitting news broadcasts or the like via the repeater and jumper either the left, right, or both channels into the audio mixer.  
  
MORE INFORMATION TO COME AS I HAVE NOT BUILT THE SHIELD YET.  
  
  
## Support
Email josh@mesilane.com.au

## Roadmap
I would like to add code to decode CTCSS tones. I have included an option for an Rx discriminator input on the crosslink side of the connector, a later version might see the last analog pin used to detect the repeater discriminator as well if needed. Im not a fan however of CTCSS decode on repeaters. I understand sometimes it's needed but for the repeater I've designed this for it's not.  
  
Open (No Tone Required) repeaters are a good thing.  

## Contributing
To contribute to this project please fork the project into your own repo and submit a merge request back to the mainstream and then email josh, josh@mesilane.com.au to review the request.

## Authors and acknowledgment
Authored by Josh Mesilane VK2MES - josh@mesilane.com.au

## License
Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]  

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

## Project status
This project is actively in development and used by the following repeaters. If you use this project for your repeater, would you please be so kind as to add your repeater information below:

VK2RKG
